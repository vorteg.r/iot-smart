#Download base image ubuntu 16.04
FROM ubuntu:16.04
# Update Ubuntu Software repository
RUN apt-get -y update
RUN apt-get -y install net-tools
RUN apt-get -y install iputils-ping
RUN apt-get -y install python3
RUN apt install -y python3-pip
RUN pip3 install sphinx
RUN pip3 install sphinx-rtd-theme
